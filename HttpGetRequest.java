package fr.open_si.Community.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.tomcat.util.codec.binary.Base64;

public class HttpGetRequest {

    private HttpURLConnection conn;
    private String userAgent;
    private String host;
    private String auth;
    private int port;
    private URL url;

    public HttpGetRequest(String host, int port, String path, Map<String, String> args) throws MalformedURLException {

        this.host = host;
        this.port = port;

        if (args != null) {
            String params = processArgs(args);
            this.url = new URL("http", this.host, this.port, path + params);
        } else {
            this.url = new URL("http", this.host, this.port, path);
        }

    }

    public void setCredits(String user, String password){
        
        // TODO use BasicAuthenticator & DigestAuthenticator
        String credits = user + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(credits.getBytes(StandardCharsets.UTF_8));
        this.auth = "Basic " + new String(encodedAuth);
        
    }
    
    public String send() throws IOException {

        this.conn = (HttpURLConnection) this.url.openConnection();
        this.conn.setDoInput(true);
        HttpURLConnection.setFollowRedirects(true);
        conn.setRequestMethod("GET");
        
        if (this.auth != null) {
            this.conn.setRequestProperty("Authorization", this.auth);
        }
        
        conn.connect();

        // Lancement requête
        String response = receive();

        conn.disconnect();
        return response;

    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setUrl(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    // --- PRIVATE METHODS ---
    private String processArgs(Map<String, String> args) {

        // args is not null so at least 1 element is present
        StringBuilder out = new StringBuilder();
        out.append("?");

        for (Entry<String, String> e : args.entrySet()) {
            out.append(e.getKey() + "=" + e.getValue());
            out.append("&");

        }

        
        // remove trailing "&"
        
        out.delete(out.length() - 1, out.length() - 1);

        return out.toString();
    }

    private String receive() throws IOException {

        StringBuilder content = null;
        String response = null;


        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {

            try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {

                String line;
                content = new StringBuilder();

                while ((line = in.readLine()) != null) {
                    content.append(line);
                }
                
                response = content.toString();
                
            }

        }

        return response;

    }

}
