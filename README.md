**QIMINFO Lab - Meylan**
--------------------------------------------------------------------------------

**Configuration centralisée**

*Objectifs :*

* centraliser la maintenance des différentes données de configuration d'une application
* les rendre accessibles au travers du réseau
* en sécuriser l'accès sans déploiement de serveur spécifique (ldap ..etc)


*Principe*

Les fichiers de configuration sont accessibles via le réseau local et téléchargés par les applications. Ces fichier sont au format json pour garantir la plus grande portabilité.

l'accès de base est de la forme http://server-ip:serveur-port/realm/ressource ou *realm* correspond au nom de l'application ou du domaine fonctionnel 
et est physiquement le répertoire racine du domaine de configuration.
On lui associe une resource qui est le nom du fichier de configuration, par exemple *database-configuration*. La *resource* est en fait le nom du fichier JSON sans l'extension .json.

Le port quant à lui représente le niveau d'environnement

* 9090 : development
* 9091 : staging
* 9092 : production


le répertoire racine des fichiers de configuration est 

`/var/configuration`

par exemple la structure des répertoires à créer pour un domaine *my-domain* sera :

* /var/configuration/my-domain/dev
* /var/configuration/my-domain/stg
* /var/configuration/my-domain/prd

on créé un fichier database-configuration.json dans chacun de ces répertoires
avec les paramètres adéquates :

```
{
  "host":"server-name/ip",
  "port":xxxx,
  "database":"my-db",
  "user":"my-user",
  "password":"my-user-password"
}
```

pour accéder aux paramètres de la base de données de l'environnement de 
développement par exemple on utilise une requête de la forme :

En navigation web :

`http://server-name/ip:9090/my-domain/database-configuration`

En ligne de commande :

`wget --user qimlab --password qiminfo http://server-name/ip:9090/my-domain/database-configuration`



Les fichiers de comptes utilisateurs sont stokés dans

`/etc/private`

et sont de la forme [realm]-[environnement] : my-domain-dev

Ce POC utilise un mode auth-basic. Pour créer un nouvel utilisateur dans un nouveau domaine en environnement de développement

`htpasswd -c /etc/private/my-domain-dev my-user`

pour ajouter / modifier un utilisateur à l'environnement de production d'un domaine existant

`htpasswd /etc/private/my-domain-prd other-user`


Installation
------------

`git clone https://gitlab.com/jeby6372/configuration-server.git`

`cd configuration-server`

`tar -xzf configuration-server_1.0.tar.gz`

`docker load < configuration-server-docker-image-1.0.tar.gz`



Démarrer le conteneur
--------
`docker run --name conf-server -d --rm -p 127.0.0.1:0:80 -p 9090:9090 -p 9091:9091 -p9092:9092 qiminfo/configuration-server:1.0`


Se connecter au nouveau conteneur pour ajouter / modifier des configurations
------------
`docker exec -it conf-server bash`


Stopper l'exécution du conteneur
-------
`docker stop conf-server`







