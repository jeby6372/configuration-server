/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.open_si.Community.classes;

import lombok.Data;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

/**
 *
 * @author emmanuel.brunet@live.fr
 */
@Data
public class PictureConfig {

    // Image dimensions
    private String fileExtension;
    private int screenMaxImageWidth;
    private int screenMaxThumbWidth;
    private int mobileMaxImageWidth;
    private int mobileMaxThumbWidth;

    public PictureConfig(String data) throws ParseException {

        JSONParser jp = new JSONParser(JSONParser.MODE_RFC4627);
        JSONObject jo = (JSONObject) jp.parse(data);

        JSONObject dimensions = (JSONObject) jo.get("gallery");
        fileExtension = dimensions.getAsString("file-ext");
        screenMaxImageWidth = Integer.valueOf(dimensions.getAsString("screen-image-width"));
        screenMaxThumbWidth = Integer.valueOf(dimensions.getAsString("screen-thumb-width"));
        mobileMaxImageWidth = Integer.valueOf(dimensions.getAsString("mobile-image-width"));
        mobileMaxThumbWidth = Integer.valueOf(dimensions.getAsString("mobile-thumb-width"));

    }

}
