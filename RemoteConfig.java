/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.open_si.Community.classes;

import fr.open_si.Community.utils.HttpGetRequest;
import java.io.IOException;
import java.net.MalformedURLException;


/**
 *
 * @author emmanuel.brunet@live.fr
 */
public class RemoteConfig {
    
    private final String HOST = "CONFIGURATION_HOST";
    private final String PORT = "CONFIGURATION_PORT";
    private final String USER = "CONFIGURATION_USER";
    private final String PASSWORD = "CONFIGURATION_PASSWORD";
    
    private String context;
    private String host;
    private int port;
    private String user;
    private String password;
    
    
    public RemoteConfig(String realm, String target){

        // reads environment variables
        
        this.host = System.getenv(HOST);
        this.port = Integer.valueOf(System.getenv(PORT));
        this.user = System.getenv(USER);
        this.password = System.getenv(PASSWORD);
        
        this.context = "/" + realm + "/" + target;
        
    }
    
    
    public String pull() throws MalformedURLException, IOException{
        HttpGetRequest request = new HttpGetRequest(this.host, this.port, this.context, null);
        request.setCredits(this.user, this.password);
        String response = request.send();
        return response.replaceAll("\\s+","");
    }
    

}
